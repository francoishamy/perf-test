export const restOptions=
{
  "single1":
  {
    vus: 1,
    iterations: 1
  },
  "single5":
  {
    vus: 1,
    iterations: 5,
  },
  "multiple5x5":
  {
    vus: 5,
    iterations: 25,
  },
  "1rq2min":
  {
    stages:
    [
      { duration: '2m', target: 1 },
      { duration: '30s', target: 0 },
    ],
  },
  "10rq2min":
  {
    stages:
    [
      { duration: '2m', target: 10 },
      { duration: '30s', target: 0 },
    ],
  },
  "50rq2min":
  {
    stages:
    [
      { duration: '2m', target: 50 },
      { duration: '30s', target: 0 },
    ],
  },
  "100rq2min":
  {
    stages:
    [
      { duration: '2m', target: 100 },
      { duration: '30s', target: 0 },
    ],
  },
  "500rq2min":
  {
    stages:
    [
      { duration: '2m', target: 500 },
      { duration: '30s', target: 0 },
    ],
  },
  "1000rq2min":
  {
    stages:
    [
      { duration: '2m', target: 1000 },
      { duration: '30s', target: 0 },
    ],
  },
  "simple100":
  {
    stages:
    [
      { duration: '30s', target: 100 },
      { duration: '4m',  target: 100 },
      { duration: '30s', target: 0 },
    ],
  },
  "simple200":
  {
    stages:
    [
      { duration: '30s', target: 200 },
      { duration: '4m',  target: 200 },
      { duration: '30s', target: 0 },
    ],
  },
  "simple400":
  {
    stages:
    [
      { duration: '30s', target: 400 },
      { duration: '4m',  target: 400 },
      { duration: '30s', target: 0 },
    ],
  },
  "slow400":
  {
    stages:
    [
      { duration: '1m',  target: 10 },
      { duration: '1m',  target: 10 },
      { duration: '2m',  target: 400 },
      { duration: '30s', target: 400 },
      { duration: '30s', target: 0 },
    ],
  },
  "simple600":
  {
    stages:
    [
      { duration: '30s', target: 600 },
      { duration: '4m',  target: 600 },
      { duration: '30s', target: 0 },
    ],
  },
  "simple800":
  {
    stages:
    [
      { duration: '30s', target: 800 },
      { duration: '4m',  target: 800 },
      { duration: '30s', target: 0 },
    ],
  },
  "simple1000":
  {
    stages:
    [
      { duration: '30s', target: 1000 },
      { duration: '4m',  target: 1000 },
      { duration: '30s', target: 0 },
    ],
  },
};

export const lightOptions = {
	stages: [
		{ duration: '10s', target: 10 },
	],
};

export const belowNormalOptions = {
	stages: [

		// 10VU during 10 seconds
		{ duration: '10s', target: 10 }, // below normal load
		{ duration: '1m', target: 20 },
		{ duration: '10s', target: 0 }, // scale down. Recovery stage.
	],
};




export const restLimitOptions = {
	stages: [

    { duration: '1m',  target: 100 },
    { duration: '1m',  target: 100 },
    { duration: '1m',  target: 200 },
    { duration: '1m',  target: 200 },
    { duration: '1m',  target: 300 },
    { duration: '1m',  target: 300 },
    { duration: '1m',  target: 400 },
    { duration: '1m',  target: 400 },
    { duration: '1m',  target: 500 },
    { duration: '5m',  target: 500 },
    { duration: '30s', target: 0 },
	],
};

export const restSingle1000Options = {
  stages: [

    { duration: '2m',  target: 1000 },
    { duration: '5m',  target: 1000 },
    { duration: '30s', target: 0 },
  ],
};
