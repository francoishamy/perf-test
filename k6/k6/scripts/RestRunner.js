import { sleep } from 'k6';

import { BASE_URLS, TOKENS } from './env.js';

export default class RestRunner {

	constructor(apiName) {
    this.apiName = apiName ;
    this.targetEnv = (`${__ENV.ENV}` !== 'undefined') ? `${__ENV.ENV}` : 'local';
		this.apiToken = TOKENS[this.targetEnv];
		this.baseUrl = BASE_URLS[this.targetEnv];
		this.path = "your_path";
	}

	runTest() {
		const requestHeaders = {
			'Authorization': 'Basic ' + this.apiToken,
			'Content-Type': 'application/json',
		};
		this.sendRequest(requestHeaders);
		sleep(1);
	}

	sendRequest(_requestHeaders) {}
}

