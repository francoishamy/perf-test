import { check, group, sleep } from 'k6';
import http from 'k6/http';
import { SharedArray } from 'k6/data';

import RestRunner from "./RestRunner.js";
import { restOptions } from "./options.js";

function getRandomInt(min=1000, max=9000) {
  return Math.floor(min + Math.random() * max);
}

class ExampleScript extends RestRunner {

  constructor() {
    super("yourApi");
    this.yourApi = new SharedArray("yourApi", function () {
      const f = JSON.parse(open('./data/example-data.json'));
        return f;
    });

  }

  sendRequest(requestHeaders) {
		group(this.apiName, () => {

      this.parameter = this.yourApi[getRandomInt(0, this.yourApi.length - 1)]; // allow you to get random data from example-data.json
      
      const response = http.batch([{
        method: "POST",
        url: `${this.baseUrl}/${this.path}`,
        body: JSON.stringify(this.parameter),
        params: { headers: requestHeaders }
      }]);
      
    
      response.forEach(function(response) {
        check(response, {
                'Response status is 200': (r)=> r.status === 200
         });
      });

      sleep(1);
		});
	}
}

const profileName = (`${__ENV.OPTIONS}` !== 'undefined') ? `${__ENV.OPTIONS}` : '1rq2min';
export let options = restOptions[profileName];
export const exampleScript = new ExampleScript();
export default () => exampleScript.runTest();
