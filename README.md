## About
This repository contains all necessary files to perfoms k6 test on a REST API.

## Set up / Prerequisites


You will need to have following tools in order to use k6 scripts : 

- docker and docker-compose installed installed
- access to internet in order to pull docker images from the docker hub.


## How to perfom test locally


#### Build and run docker services

#### docker-compose.yml

The docker-compose configuration file defines three servers and two networks, combining them together into a solution comprising a visualisation web server, database and load test client (source: [here](https://medium.com/swlh/beautiful-load-testing-with-k6-and-docker-compose-4454edb3a2e3)):

- Runs Grafana web server for visualisation in the background on port 3000
- Runs InfluxDB database in the background on port 8086
- Runs K6 on an ad-hoc basis to execute a load test script

Run ``` docker compose up -d ``` to build 3 services (described into [docker-compose.yaml](./docker-compose.yml)).

Now, you should have 2 containers running:
```(shell)
francois.hamy@hafr:~/Desktop/HAFR/CDA/perf/k6 » docker ps 
CONTAINER ID   IMAGE                    COMMAND                  CREATED          STATUS          PORTS                    NAMES
376e10f66228   grafana/grafana:latest   "/run.sh"                12 seconds ago   Up 11 seconds   0.0.0.0:3000->3000/tcp   k6-grafana-1
5286a3517180   influxdb:1.8             "/entrypoint.sh infl…"   12 seconds ago   Up 10 seconds   0.0.0.0:8086->8086/tcp   k6-influxdb-1
```

- **grafana** : supports InfluxDB as a data source. You will be able to visualize performance results on a dashboard named " *k6 Load Testing Results* "(http://localhost:3000/)
)
- **influxdb** : is a fast time-series database which is supported by K6 as an output target for realtime monitoring of a test. Whilst K6 is running the load test, it will stream statistics about the run to InfluxDB.

#### Perfom k6 tests

You are now ready to perform k6 test on a givenservice:

##### Performance test on given enpoint 

```(shell)
docker compose run k6 run /scripts/ExampleScript.js --iterations 2 --vus 1 -e ENV=dev -e TEST_CASE=your_test_case
```

**expected result** : (also check http://localhost:3000) 

```(shell)
francois.hamy@hafr:~/Desktop/HAFR/CDA/perf/k6 » docker compose run k6 run /scripts/ExampleScript.js --iterations 2 --vus 1 -e ENV=dev -e TEST_CASE=your_test_case

          /\      |‾‾| /‾‾/   /‾‾/   
     /\  /  \     |  |/  /   /  /    
    /  \/    \    |     (   /   ‾‾\  
   /          \   |  |\  \ |  (‾)  | 
  / __________ \  |__| \__\ \_____/ .io

  execution: local
     script: /scripts/ExampleScript.js
     output: InfluxDBv1 (http://influxdb:8086)

  scenarios: (100.00%) 1 scenario, 1 max VUs, 10m30s max duration (incl. graceful stop):
           * default: 2 iterations shared among 1 VUs (maxDuration: 10m0s, gracefulStop: 30s)


running (00m04.5s), 0/1 VUs, 2 complete and 0 interrupted iterations
default ✓ [======================================] 1 VUs  00m04.5s/10m0s  2/2 shared iters

     █ example

       ✓ Response status is 200

     checks.........................: 100.00% ✓ 2        ✗ 0  
     data_received..................: 6.2 kB  1.4 kB/s
     data_sent......................: 959 B   214 B/s
     group_duration.................: avg=1.22s    min=1.08s    med=1.22s    max=1.35s    p(90)=1.32s    p(95)=1.34s   
     http_req_blocked...............: avg=88.72ms  min=188.25µs med=88.72ms  max=177.26ms p(90)=159.55ms p(95)=168.41ms
     http_req_connecting............: avg=9.12ms   min=0s       med=9.12ms   max=18.24ms  p(90)=16.42ms  p(95)=17.33ms 
     http_req_duration..............: avg=120.54ms min=80.54ms  med=120.54ms max=160.54ms p(90)=152.54ms p(95)=156.54ms
       { expected_response:true }...: avg=120.54ms min=80.54ms  med=120.54ms max=160.54ms p(90)=152.54ms p(95)=156.54ms
     http_req_failed................: 0.00%   ✓ 0        ✗ 2  
     http_req_receiving.............: avg=5.12ms   min=1.94ms   med=5.12ms   max=8.31ms   p(90)=7.67ms   p(95)=7.99ms  
     http_req_sending...............: avg=832.37µs min=275.33µs med=832.37µs max=1.38ms   p(90)=1.27ms   p(95)=1.33ms  
     http_req_tls_handshaking.......: avg=51.01ms  min=0s       med=51.01ms  max=102.02ms p(90)=91.82ms  p(95)=96.92ms 
     http_req_waiting...............: avg=114.58ms min=71.95ms  med=114.58ms max=157.21ms p(90)=148.68ms p(95)=152.95ms
     http_reqs......................: 2       0.445222/s
     iteration_duration.............: avg=2.23s    min=2.09s    med=2.23s    max=2.36s    p(90)=2.34s    p(95)=2.35s   
     iterations.....................: 2       0.445222/s
     vus............................: 1       min=1      max=1
     vus_max........................: 1       min=1      max=1
     
```

#### Variables explanations

- **VUS** : Virtual Users are an emulation of a user performing your script. You can consider them to be an execution agent which has loaded your script and its context and is responsible for running it. They load your script and run it, that’s it. In practice, if you ask k6 to run N VUs, it will make sure to spawn N concurrent components running your script repeatedly; effectively simulating N users performing the behavior you described in your script repeatedly.
- **iterations** : is the number of request iteration made by a VU.